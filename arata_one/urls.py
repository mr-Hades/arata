"""arata_one URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static

from maindb.views import login_view, home_view, \
                         about_view, shop_server_view, shop_card_loading_view, \
                         market_checkout_view, market_login_view, market_payment_view, \
                         shop_card_updating_view, shop_refunding_view, lockout_view, \
                         partners_view, activation_view

urlpatterns = [
	url(r'^admin/', admin.site.urls),
    url(r'^admin/defender/', include('defender.urls')),
	url(r'^login/', login_view, name='login'),
    url(r'^about/', about_view),
	url(r'^home/', home_view),
	url(r'^$', home_view),
    url(r'^shops/transactions/$', shop_server_view),
    url(r'^shops/card_loading/$', shop_card_loading_view),
    url(r'^shops/card_updating/$', shop_card_updating_view),
    url(r'^shops/refunding/$', shop_refunding_view),
    url(r'^market/login/$', market_login_view),
    url(r'^market/checkout/$', market_checkout_view),
    url(r'^market/payments/$', market_payment_view),
    url(r'^locked/$', lockout_view),
    url(r'^partners/((?P<card_type>[-\w]+)/)?$', partners_view),
    url(r'^activation/$', activation_view),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

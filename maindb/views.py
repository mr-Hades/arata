from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, Http404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, logout
from django.shortcuts import render,redirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.signals import user_logged_in,\
    									user_logged_out,\
    									user_login_failed
from django.utils import timezone
from .auth_backend import CardPhoneAuth
from .models import Balance, Transaction, User, Card, Shop, \
					CardType, CardTypeShop, PendingCardUpdate

from .forms import CaptchaForm, ActivationCardForm, ActivationUserForm

from django.conf import settings
from defender.decorators import watch_login
from datetime import datetime, timedelta
from math import ceil

def lockout_view(request):
	return render(request, 'templates/lockout.html')


@csrf_exempt
def shop_card_loading_view(request):
	if request.POST:
		try:
			shop_id = request.POST.get('shop_id')
			shop_key = request.POST.get('shop_key')
			shop_uuid = request.POST.get('shop_uuid')

			# check if shop is secure
			# if we can get it without exceptions then it is
			shop = Shop.objects.get(shop_idx=shop_id,server_uid=shop_uuid,key=shop_key)

			card_id = request.POST.get('card_id')

			val_list = ('id','physical_id','user__first_name',
						 'user__second_name','user__birthday', 'user__uid', 'card_active'
						)

			q = Card.objects.filter(id__gt=int(card_id), staff=False).\
							values_list(*val_list).order_by('id')

			response = {}
			response['cards'] = list(q)

		except Exception as e:
			print('=========================shop_server_loading_view')
			print(e)
			return HttpResponse(e)

		return JsonResponse(response)

	return HttpResponse()


@csrf_exempt
def shop_card_updating_view(request):
	if request.POST:
		try:
			shop_id = request.POST.get('shop_id')
			shop_key = request.POST.get('shop_key')
			shop_uuid = request.POST.get('shop_uuid')
			# check if shop is secure
			# if we can get it without exceptions then it is
			shop = Shop.objects.get(shop_idx=shop_id,server_uid=shop_uuid,key=shop_key)

			card_query = PendingCardUpdate.objects.filter(shop=shop,card__staff=False)

			val_list = ('action__name', 'card__id','card__physical_id', 
						'card__user__first_name', 'card__user__second_name', 
						'card__user__birthday',	'card__user__uid', 
						)

			q = card_query.values_list(*val_list).order_by('card__id')
			
			response = {}
			response['cards'] = list(q)
			if card_query:
				card_query.delete()

		except Exception as e:
			print('=========================shop_server_updating_view')
			print(e)
			return HttpResponse(e)

		return JsonResponse(response)

	return HttpResponse()


@csrf_exempt
def market_login_view(request):
	if request.POST:
		uid = request.POST.get('key')
		try:
			shop = Shop.objects.get(server_uid=uid)
		except Exception as e:
			return HttpResponse('error')
		card_number = request.POST.get('card_num')
		phone_number = request.POST.get('phone_num')
		try:
			card = Card.objects.get(card_number=card_number)
			user = User.objects.get(phone_number=phone_number,id=card.user.id)
			balance = Balance.objects.get(card=card)
			return JsonResponse({'name': user.first_name, 'balance': balance.summ_pts})
		except (User.DoesNotExist, Card.DoesNotExist):
			return JsonResponse({})

	return HttpResponse()


@csrf_exempt
def market_payment_view(request):
	if request.POST:
		try:
			uid = request.POST.get('key')
			try:
				shop = Shop.objects.get(server_uid=uid)
			except Exception as e:
				print("===================== market_payment_view")
				print(e)
				return HttpResponse('error')
			
			card_number = request.POST.get('card_num')
			months = request.POST.get('month')

			card = Card.objects.get(card_number=card_number)
			extra_months = timedelta(30 * int(months))
			if card.card_active:
				card.expiration_date += extra_months
			else:
				shops_list = CardTypeShop.objects.filter(card_type=card.type)
				for shop_item in shops_list:
					PendingCardUpdate(shop=shop_item.shop, card=card).save()

				card.expiration_date = datetime.now() + extra_months
				card.card_active = True
			
			card.save()

			return HttpResponse('ok')

		except ValueError:
			print("===================== market_payment_view")
			print('bad api key')

		except Exception as e:
			print("===================== market_payment_view")
			print(e)

	return HttpResponse('error')


@csrf_exempt
def market_checkout_view(request):
	if request.POST:
		card_number = request.POST.get('card_id')
		summ = request.POST.get('summ')
		uid = request.POST.get('key')
		try:
			shop = Shop.objects.get(server_uid=uid)
		except Exception as e:
			return HttpResponse('error')

		try:
			card = Card.objects.get(card_number=card_number, card_active=True)
			balance = Balance.objects.get(card=card)
			f_sum = float(summ)
			balance.summ_pts -= f_sum
			if balance.summ_pts >= 0:
				balance.save()
			else:
				return HttpResponse('insufficient funds')
			date_time = timezone.now()
			Transaction(shop=shop, card = card, type='B', 
						date_time=date_time, value=-f_sum*settings.COIN_DRAM_COEF,
					 	bonus_points=-f_sum).save()

			return HttpResponse('ok')
		except (User.DoesNotExist, Card.DoesNotExist):
			return HttpResponse('error')
	return HttpResponse('')


@watch_login()
def login_view(request):
	logout(request)
	card_number = ''
	phone_number= ''
	try:
		if request.POST:
			card_number = request.POST.get('card_number')
			phone_number = request.POST.get('phone_number')

			user = CardPhoneAuth().authenticate(card_number=card_number, phone_number=phone_number)
			if user and user.is_active:
				login(request, user, backend='maindb.auth_backend.CardPhoneAuth',)
				user_logged_in.send(
									sender = Card,
									request = request,
									user = user,
									)
				request.session['card_number']=card_number
				return HttpResponseRedirect('/home/')

			user_login_failed.send(
						sender = Card,
						request = request,
						credentials = {'username':card_number},
					)

	except Exception as e:
		print('==============login_view')
		print(e)

	return render(request,'templates/wrong_login.html',{'username':card_number,'password':phone_number})


def about_view(request):
	return render(request, 'templates/about-us.html')


@login_required(login_url='/login/')
def home_view(request):
	if request.POST:
		try:
			if request.POST.get('activated') == '1':
				card_number = request.session['card_number']
				card = Card.objects.get(card_number=card_number)
				card.card_is_activated = True
				card.save()
		except Exception as e:
			print("=================== home_view card activation")
			print(e)

	values = {}

	try:
		page_ind = request.GET.get('p')

		if page_ind and int(page_ind) > 0:

			card_number = request.session['card_number']
			card = Card.objects.get(card_number=card_number)

			values['BALANCE'] = Balance.objects.get(card=card).summ_pts
			values['activated'] = card.card_is_activated
			
			trans_per_page = 10

			filtered_vals = Transaction.objects.filter(card=card).order_by('-date_time')
			values['pages_num'] = str(ceil(len(filtered_vals) / trans_per_page ))

			ind = trans_per_page*(int(page_ind)-1)
			ziped_list = [[t_list.date_time, str(t_list.shop), t_list.value, t_list.bonus_points]
				for t_list in filtered_vals[ind:ind+trans_per_page]]

			values['ziped_list'] = ziped_list

		else:
			return HttpResponseRedirect("/home/?p=1")

	except Exception as e:
		print('================home view')
		print(e)

	return render(request, 'templates/home.html', values)


@csrf_exempt
def shop_server_view(request):
	try:
		shop_id = request.POST.get('shop_id')
		shop_uuid = request.POST.get('shop_uuid')
		shop_key = request.POST.get('shop_key')
		card_id = request.POST.get('card_id')
		money = request.POST.get('money_val')
		time = request.POST.get('time')
		trans = request.POST.get('transaction_id')
		trans_type = request.POST.get('type')


		shop = Shop.objects.get(shop_idx=shop_id,server_uid=shop_uuid,key=shop_key)
		card = Card.objects.get(physical_id=card_id, card_active=True)

		# check if card type is supported by shop
		c_type = CardTypeShop.objects.get(shop=shop, card_type=card.type)
		
		# needed for transaction id check if
		if Transaction.objects.filter(shop=shop, card=card, type=trans_type,
									  transaction_number=trans):
			return HttpResponse('ok')

		# c_type = CardType.objects.get(id=card.type.id)
		dis_percent = c_type.discount
		f_money = float(money)

		if trans_type == 'R':
			f_money = -f_money

		pts = f_money / settings.COIN_DRAM_COEF * dis_percent / 100
		
		Transaction(shop=shop, card=card, 
					date_time=time, value=f_money, type=trans_type, 
					bonus_points=pts, transaction_number=trans
					).save()

		balance = Balance.objects.get(card=card)
		balance.summ_money += f_money
		balance.summ_pts += pts
		balance.save()

		return HttpResponse('ok')

	except Exception as e:
		print('=========================shop_server_view')
		print(e)
		return HttpResponse('bad card')

	# bad response handling fucker
	return HttpResponse('error')


@csrf_exempt
def shop_refunding_view(request):
	return HttpResponse("ok")

def partners_view(request,card_type=None):
	if card_type:
		try:
			val_list = ('shop__name',)
			partners = CardTypeShop.objects.filter(shop__logo__isnull=False,
										   		   card_type__name = card_type,
										  		  ).exclude(shop__name='base_shop')
			return render(request,"templates/partners.html",{'partners':partners})
		except:
			pass

	partners = CardTypeShop.objects.filter(shop__logo__isnull=False
								  		  ).exclude(shop__name='base_shop')
	return render(request,"templates/partners.html",{'partners':partners})


def activation_view(request):
	if request.POST:
		form_card = ActivationCardForm(request.POST)
		form_user = ActivationUserForm(request.POST)
		form_cap = CaptchaForm(request.POST)
		form_dic = {'form_cap':form_cap,
						'form_card':form_card, 
						'form_user':form_user
						}


		# Validate the form: the captcha field will automatically
		# check the input
		if form_card.is_valid() and form_user.is_valid() and form_cap.is_valid():
			
			try:
				card = Card.objects.get(card_number=form_card.cleaned_data['card_number'])
				if card.card_is_activated:
					return render(request, 'templates/activation_failed_card_already_activated.html',form_dic)
			except:
				return render(request, 'templates/activation_failed_bad_card.html',form_dic)

			try:
				user = form_user.save()

				card.user = user
				Balance(card=card).save()
				card.card_is_activated = True
				card.save()
			except:
				pass

			return render(request, 'templates/login.html')
		else:
			return render(request, 'templates/activation_failed_bad_phone_number.html',form_dic)

	else:
		form_cap = CaptchaForm()
		form_card = ActivationCardForm()
		form_user = ActivationUserForm()

	form_dic = {'form_cap':form_cap,
				'form_card':form_card, 
				'form_user':form_user
				}

	return render(request, 'templates/activation.html',form_dic)

from django.db import models
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from uuid import uuid1, uuid4

import random
import string

def random_word(length=64):
	return ''.join([random.choice(string.ascii_letters + string.digits)\
					 for i in range(length)])

class User(models.Model):
	first_name = models.CharField(max_length=30)
	second_name = models.CharField(max_length=30)
	patronymic = models.CharField(max_length=30, default = '')
	phone_number = models.CharField(max_length=11, unique=True)
	address = models.CharField(max_length=50)
	city = models.CharField(max_length=30)
	country = models.CharField(max_length=40)
	uid = models.CharField(max_length=40, default=uuid4)

	email = models.CharField(max_length=40)
	date_joined = models.DateField(auto_now_add=True,null=True, blank=True)
	birthday = models.DateField(null=True)

	def __str__(self):
		return self.first_name + ' ' + self.second_name


class CardType(models.Model):
	name = models.CharField(max_length = 20, null=False, blank=False,)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name_plural = "Card Types"


class CardManager(BaseUserManager):
	def create_user(self, card_number, password=''):
		"""
		Creates and saves a User with the given email and password.
		"""
		if not card_number:
			raise ValueError('Users must have a card number')

		user = self.model(card_number=card_number)

		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_staffuser(self, card_number, password):
		"""
		Creates and saves a staff user with the given email and password.
		"""
		user = self.create_user(
								card_number,
								password=password,
								)
		user.staff = True
		user.save(using=self._db)
		return user

	def create_superuser(self, card_number, password):
		"""
		Creates and saves a superuser with the given email and password.
		"""
		user = self.create_user(
								card_number,
								password=password,
								)
		user.staff = True
		user.admin = True
		user.save(using=self._db)
		return user


class Card(AbstractBaseUser):
	user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
	type = models.ForeignKey(CardType, null=True, blank=True, on_delete=models.CASCADE)

	physical_id = models.CharField(max_length=16, null=True, blank=True)
	creation_date = models.DateField(auto_now_add=True,null=True, blank=True)
	expiration_date = models.DateField(null=True, blank=True)

	# this fields are used for user authentification implementation
	# https://www.codingforentrepreneurs.com/blog/how-to-create-a-custom-django-user-model/
	card_number = models.CharField(max_length=16, null=True, unique=True)
	active = models.BooleanField(default=True)
	staff = models.BooleanField(default=False) # a admin user; non super-user
	admin = models.BooleanField(default=False) # a superuser
	# notice the absence of a "Password field", that's built in.
	card_active = models.BooleanField(default=False)
	card_is_activated = models.BooleanField(default=False)

	USERNAME_FIELD = 'card_number'
	REQUIRED_FIELDS = [] # Email & Password are required by default.
	objects = CardManager()

	def __str__(self):
		return self.card_number

	def get_full_name(self):
		# The user is identified by their email address
		return self.card_number

	def get_short_name(self):
		# The user is identified by their email address
		return self.card_number

	def has_perm(self, perm, obj=None):
		"Does the user have a specific permission?"
		# Simplest possible answer: Yes, always
		return True

	def has_module_perms(self, app_label):
		"Does the user have permissions to view the app `app_label`?"
		# Simplest possible answer: Yes, always
		return True

	@property
	def is_staff(self):
		"Is the user a member of staff?"
		return self.staff

	@property
	def is_admin(self):
		"Is the user a admin member?"
		return self.admin

	@property
	def is_active(self):
		"Is the user active?"
		return self.active


class Balance(models.Model):
	card = models.ForeignKey(Card,on_delete=models.CASCADE)
	summ_money = models.FloatField(default=0)
	summ_pts = models.FloatField(default=0)

	def __str__(self):
		return str(self.card) + ' ' + str(self.summ_pts)


class Payment(models.Model):
	card = models.ForeignKey(Card,on_delete=models.CASCADE)
	date = models.DateTimeField()
	reference = models.CharField(max_length=20)
	value = models.IntegerField()


class Shop(models.Model):
	name = models.CharField(max_length = 40, blank=True)
	logo = models.ImageField(upload_to = 'logos/', null=True)
	address = models.CharField(max_length = 50)
	shop_idx = models.CharField(max_length=40, default=uuid1, unique=True)
	server_uid = models.CharField(max_length=100, default=random_word)
	key = models.CharField(max_length=40, default=uuid4, unique=True)
	active = models.BooleanField(default=True)

	def __str__(self):
		return self.name + ', ' + self.address


class Transaction(models.Model):
	shop = models.ForeignKey(Shop,on_delete=models.CASCADE)
	card = models.ForeignKey(Card,on_delete=models.CASCADE)

	transaction_number = models.IntegerField(null=True)
	date_time = models.DateTimeField()
	value = models.FloatField(default=0)
	bonus_points = models.FloatField(default=0)
	type = models.CharField(max_length=2, default='P')
	# class Meta:
	# 	verbose_name_plural = "Transactions"


class CardTypeShop(models.Model):
	card_type = models.ForeignKey(CardType, null=False, blank=False, on_delete=models.CASCADE)
	shop = models.ForeignKey(Shop, null=False, blank=False, on_delete=models.CASCADE)
	discount = models.IntegerField(default=0)	#in percents

	class Meta:
		verbose_name_plural = "shop - card type"


class ActionName(models.Model):
	"""docstring for ActionTypes"""
	name = models.CharField(max_length=20, null=True)
	# action = models.ForeignKey(Actions,on_delete=models.CASCADE)
	def __str__(self):
		return str(self.name)
		

class PendingCardUpdate(models.Model):
	card = models.ForeignKey(Card, null=True, blank=True, on_delete=models.CASCADE)
	shop = models.ForeignKey(Shop, null=True, blank=True, on_delete=models.CASCADE)
	action = models.ForeignKey(ActionName, null=True, blank=True, on_delete=models.CASCADE)

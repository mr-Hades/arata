from django.contrib import admin

# Register your models here.
from .models import User, Card, Balance, Payment, Shop, Transaction, \
                    CardType, CardTypeShop, PendingCardUpdate, ActionName 


class UserAdmin(admin.ModelAdmin):
	list_display = [f.name for f in User._meta.fields if f.name != 'id']


class BalanceAdmin(admin.ModelAdmin):
	list_display = [f.name for f in Balance._meta.fields if f.name != 'id']


class PaymentAdmin(admin.ModelAdmin):
	list_display = [f.name for f in Payment._meta.fields if f.name != 'id']


class ShopAdmin(admin.ModelAdmin):
	list_display = [f.name for f in Shop._meta.fields if f.name != 'id']


class TransactionAdmin(admin.ModelAdmin):
	list_display = [f.name for f in Transaction._meta.fields if f.name != 'id']


class CardTypeAdmin(admin.ModelAdmin):
    list_display = [f.name for f in CardType._meta.fields if f.name != 'id']


class CardTypeShopAdmin(admin.ModelAdmin):
    list_display = [f.name for f in CardTypeShop._meta.fields if f.name != 'id']


class PendingCardUpdateAdmin(admin.ModelAdmin):
    list_display = [f.name for f in PendingCardUpdate._meta.fields if f.name != 'id']

class ActionNameAdmin(admin.ModelAdmin):
    list_display = [f.name for f in ActionName._meta.fields if f.name != 'id']    

from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .forms import CardAdminCreationForm, CardAdminChangeForm

class CardAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = CardAdminChangeForm
    add_form = CardAdminCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ['card_number', 'admin'] +\
         [f.name for f in Card._meta.fields if f.name != 'id' and
          f.name != 'card_number' and f.name != 'admin' and f.name != 'creation_date']

    other_fields = ('last_login', 'user', 'physical_id', 'type', 
                    'expiration_date', 'active', 'card_active', 'card_is_activated')

    list_filter = ('admin',)
    fieldsets = (
        (None, {'fields': ['card_number', 'password']}),
        ('Personal info', {'fields': other_fields}),
        ('Permissions', {'fields': ('admin','staff')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('card_number', 'password1', 'password2')}
        ),
    )
    search_fields = ('card_number',)
    ordering = ('card_number',)
    filter_horizontal = ()



# Remove Group Model from admin. We're not using it.
admin.site.unregister(Group)

admin.site.register(User, UserAdmin)
admin.site.register(Card, CardAdmin)
admin.site.register(Balance, BalanceAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(Shop, ShopAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(CardType, CardTypeAdmin)
admin.site.register(CardTypeShop, CardTypeShopAdmin)
admin.site.register(PendingCardUpdate, PendingCardUpdateAdmin)
admin.site.register(ActionName, ActionNameAdmin)

from django.contrib.auth.backends import ModelBackend
from .models import Card, User


class CardPhoneAuth(ModelBackend):
    def authenticate(self, card_number=None, phone_number=None):
        try:
            card = Card.objects.get(card_number=card_number)
            User.objects.get(phone_number=phone_number,id=card.user.id)
            return card
        except (User.DoesNotExist, Card.DoesNotExist):
            return None

    def get_user(self, card_id):
        try:
            return Card.objects.get(pk=card_id)
        except Card.DoesNotExist:
            return None
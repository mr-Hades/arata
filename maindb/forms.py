# accounts.forms.py
from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from .models import Card, User
from django.forms.extras.widgets import SelectDateWidget
from captcha.fields import ReCaptchaField
import datetime

class RegisterForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput, required=False)
    password2 = forms.CharField(label='Confirm password', widget=forms.PasswordInput, required=False)

    class Meta:
        model = Card
        fields = ('card_number',)

    def clean_card_number(self):
        card_number = self.cleaned_data.get('card_number')
        qs = User.objects.filter(card_number=card_number)
        if qs.exists():
            raise forms.ValidationError("card_number is taken")
        return card_number

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2


class CardAdminCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput, required=False)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput, required=False)

    class Meta:
        model = Card
        fields = ['card_number'] +\
             [f.name for f in Card._meta.fields 
                if f.name != 'id' and f.name != 'card_number' and
                 f.name != 'creation_date' and f.name != 'creation_date']

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(CardAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class CardAdminChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Card
        fields = ('card_number', 'password', 'active', 'admin')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class ActivationCardForm(forms.Form):
    card_number = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Քարտի ութանիշ թիվ',
                                         'name': 'Քարտի ութանիշ թիվ',
                                         'id':'id_activate_user_card_number',
                                         'class': 'form-control',
                                         'type': 'tel',
                                         'label': 'Քարտի ութանիշ թիվ',
                                         'oninvalid': "this.setCustomValidity('Լրացրեք դաշտը')",
                                         'oninput': "setCustomValidity('')",
                                         }))


class ActivationUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name','second_name', 'patronymic',
                  'phone_number', 'city', 'address', 'email','birthday')

        labels = {'first_name' : 'Անուն','second_name':'Ազգանուն',
                  'patronymic' : 'Հայրանուն','phone_number':'Հեռախոսահամար',
                  'address':'Հասցե', 'city':'Քաղաք',
                  'email':'Էլեկրոնային փոստ', 'birthday': 'Ծննդյան օր'}

        widgets= {}

        for item in labels:
            widgets[item] = forms.TextInput(attrs={'placeholder':labels[item],
                                                   'name':labels[item],
                                                   'id':'id_activate_user_'+ item,
                                                   'class': 'form-control',
                                                   'oninvalid': "this.setCustomValidity('Լրացրեք դաշտը')",
                                                   'oninput': "setCustomValidity('')",
                                                   })
        
        item= 'phone_number'
        widgets[item] = forms.TextInput(attrs={'placeholder':labels[item],
                                                   'name':labels[item],
                                                   'id':'id_activate_user_'+ item,
                                                   'class': 'form-control',
                                                   'type': 'tel',
                                                   'oninvalid': "this.setCustomValidity('Լրացրեք դաշտը')",
                                                   'oninput': "setCustomValidity('')",
                                                   })
        item= 'email'    
        widgets[item] = forms.TextInput(attrs={'placeholder':labels[item],
                                                   'name':labels[item],
                                                   'id':'id_activate_user_'+ item,
                                                   'class': 'form-control',
                                                   'type': 'email',
                                                   'oninvalid': "this.setCustomValidity('Լրացրեք դաշտը')",
                                                   'oninput': "setCustomValidity('')",
                                                   })

        item = 'birthday'
        widgets[item] = SelectDateWidget(attrs={'placeholder':labels[item],
                                                   'name':labels[item],
                                                   'id':'id_activate_user_'+ item,
                                                   'class': 'form-control no-100 d-flex p-2',
                                                   },
                                          years= range(datetime.date.today().year, datetime.date.today().year -150, -1),
                                        )
         # forms.DateInput(attrs={'placeholder':labels[item],
         #                                           'name':labels[item],
         #                                           'id':'id_activate_user_'+ item,
         #                                           'class': 'form-control',
         #                                           'type': 'text',
         #                                           'onfocus': "(this.type='date')",
         #                                           'onblur': "(this.type='text')",
         #                                           'oninvalid': "this.setCustomValidity('Լրացրեք դաշտը')",
         #                                           'oninput': "setCustomValidity('')",
         #                                           })


class CaptchaForm(forms.Form):
    captcha = ReCaptchaField(attrs={'theme' : 'clean',}, label="")
